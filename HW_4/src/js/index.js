const books = [
    {
        author: "Lucy Foley",
        name: "List of Invitees",
        price: 70
    },
    {
        author: "Susanna Clarke",
        name: "Jonathan Strange & Mr Norrell",
    },
    {
        name: "Design. A Book for Non-Designers.",
        price: 70
    },
    {
        author: "Alan Moore",
        name: "Neonomicon",
        price: 70
    },
    {
        author: "Terry Pratchett",
        name: "Moving Pictures",
        price: 40
    },
    {
        author: "Angus Hyland",
        name: "Cats in Art",
    }
];



const root = document.getElementById("root")
root.innerHTML = `<ul class="books-container"></ul>`


let content = ''
const createObject = (data) => {

    const book = `
        <li class="book">
            <h3 class="book__name">${data.getName}</h3>
            
            <div class="book__author">
                <i class="book__author__title">Author:</i>
                <h4 class="book__author__name">${data.getAuthor}</h4>
            </div>
            <div class="book__price">
                <i class="book__price__title">Price:</i>
                <p class="book__price__name">${data.getPrice} <i>pounds</i></p>
            </div>
        </li>
    
    `

    content += book
}


books.map(book => {
    let data = new Book(book.author, book.name, book.price)
    if (data.isValidObject()) {
        
        createObject(data)
    }
})
    
const booksContainer = document.querySelector('.books-container')
booksContainer.innerHTML = content