class Book {


    #author 
    #name
    #price


    constructor(author, name, price) {
        this.#author = author
        this.#name = name
        this.#price = price
    }



    checkProperty = (property, value) => {
        if (property === undefined){
            console.log(`${value} is missing in the object`)
            return false
        }
        return true
    }


    isValidObject = () => {
        try {
           
            
            if (!this.#author) {
                throw new Error(`An author property is missing in the object`);
            }
            
            if (!this.#name) {
                throw new Error(`A name property is missing in the object`);
            }
            
            if (!this.#price) {
                throw new Error(`A price property is missing in the object`);
            }
            
            return true
        } catch (error) {
            console.warn(error.message);
            return false
        }
        

    }

    


    get getAuthor() {
        return this.#author
    }

    set setAuthor(author) {
        this.#author = author
    }



    get getName() {
        return this.#name
    }

    set setName(name){
        this.#name = name
    }



    get getPrice(){
        return this.#price
    }

    set setPrice(price){
        this.#price = price
    }   
}

