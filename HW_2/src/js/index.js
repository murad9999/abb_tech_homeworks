const instaHeader = document.getElementById('insta-head')

window.addEventListener('resize',  () => {
    if (window.innerWidth >= 768){
        instaHeader.innerHTML = '- Latest Instagram Shots'
    }

    if (window.innerWidth < 768){
        instaHeader.innerHTML = '- Last Instagram Shots'
    }
})