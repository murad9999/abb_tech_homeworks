

const menu = document.getElementById('menu')
const menuNav = document.getElementById('menu-nav')

menu.addEventListener('click', () => {
        menu.classList.toggle('menu-active')
        menuNav.classList.toggle('active-menu-nav')
})
