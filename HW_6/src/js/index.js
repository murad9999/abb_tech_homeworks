let MOVIES_URL = "https://ajax.test-danit.com/api/swapi/films"

async function getData(url) {
    const res = await fetch(url)
    const data = await res.json()
    return data
}


function setLoad(el) {
    el.innerHTML = `<div class="loader-container"><span class="loader"></span></div>`
}


let movies = await getData(MOVIES_URL)
const moviesMenu = document.querySelector('.movies')
movies.sort((a, b) => a.episodeId > b.episodeId ? 1 : -1)

movies.map(movie => {
    moviesMenu.insertAdjacentHTML('beforeend', `
    <li class=" movie__container">
        <div class="movie">
            <img src="https://yt3.googleusercontent.com/NLJIsq7K-Qr7AMpHkLstcm9F_ZQzel_CYngyfJvAuBoOzyICVBlpXZzmGlMFqhD1PoV1bJwoxyk=s900-c-k-c0x00ffffff-no-rj"/>
            <div class="movie__detail">
                <h3 class="movie__name">${movie.name}</h3>
                <div class="movie__desc">${movie.openingCrawl}</div>
                <span class="movie__id">Episode: ${movie.episodeId}</span>
            </div>
        </div>
        <div class="characters characters-${movie.episodeId}">
            
        </div>
    </li>
    `)
    const charactersContainer = document.querySelector(`.characters-${movie.episodeId}`)
   
    setLoad(charactersContainer)
    
    Promise.all(movie.characters.map(async(characterUrl) => {
        return await getData(characterUrl) 
    })).then(characters => {
  
        let content = characters.map(c => `<p class="character">${c.name}</p>`).join('')
        charactersContainer.innerHTML = `<h3 class="characters__heading">Characters</h3><div class="character__details">${content}</div>`
    }).catch(err => {
        console.error(err)
    })
})

