const Employee = require('./Employee.js')
const Programmer = require('./Programmer.js')





const programmer_1 = new Programmer("Murad Masimli", 20, 500, ['Python', 'Javascript'])
let salary = programmer_1.getSalary
let name = programmer_1.getName
programmer_1.setAge = 21
let age = programmer_1.getAge
let langs = programmer_1.getLang
console.log(`Programmer: ${name} -> Salary: ${salary} -> Age: ${age}`)
console.log(`${name} is good at these languages ->${langs.map(l => ` ${l}`)}`)


/* DIVIDER */
console.log('\n')

const programmer_2 = new Programmer("Sahib Humbatzada", 22, 700, ['C#'])
salary = programmer_2.getSalary
name = programmer_2.getName
age = programmer_2.getAge
langs = programmer_2.getLang
console.log(`Programmer: ${name} -> Salary: ${salary} -> Age: ${age}`)
console.log(`${name} is good at ${langs.length > 1 ? "these languages" : "this language"} ->${langs.map(l => ` ${l}`)}`)