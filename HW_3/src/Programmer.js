const Employee = require("./Employee");

class Programmer extends Employee{
    #lang
    constructor(name, age, salary, lang) {
        super(name, age, salary)
        this.#lang = lang
    }

    /* OVERRIDING SALARY */
    get getSalary(){
        return super.getSalary * 3
    }



    /* LANG */
    get getLang() {
        return this.#lang
    }

    set setLang(lang) {
        this.#lang = lang
    }



}

module.exports = Programmer