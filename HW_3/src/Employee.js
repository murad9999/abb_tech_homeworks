class Employee {
    #name
    #age
    #salary

    constructor(name, age, salary) {
        this.#name = name;
        this.#age = age;
        this.#salary = salary;

    }

    /* NAME */
    get getName() {
        return this.#name
    }

    set setName(name) {
        this.#name = name
    }

    /* AGE */
    get getAge() {
        return this.#age
    }

    set setAge(age) {
        this.#age = age
    }

    /* SALARY */
    get getSalary() {
        return this.#salary
    }

    set setSalary(salary) {
        this.#salary = salary
    }

}

module.exports = Employee
