const array = ['value', () => 'showValue'];

const [value, showValue] = array

alert(value); // should output 'value'
alert(showValue()); // should output 'showValue'