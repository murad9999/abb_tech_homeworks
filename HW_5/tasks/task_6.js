const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}

const modifiedEmployee = {
    ...employee, 
    age: 28,
    salary: 3000
}

console.log(modifiedEmployee)
