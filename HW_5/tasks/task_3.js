const user1 = {
    name: "John",
    years: 30,
  
};


const {name: name, years: age, isAdmin = false} = user1
console.log(`Name: ${name}\nAge: ${age}\nAdmin role: ${isAdmin}`)


