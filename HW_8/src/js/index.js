IP_URL = 'https://api.ipify.org/?format=json'
IP_API_URL = 'http://ip-api.com/json'


async function getData(url) {
    try {
        const res = await fetch(url)
        const data = await res.json()
        return data
    } catch (err) {
        throw new Error(err)
    }
}




const btn = document.querySelector('.btn-ip')

btn.addEventListener('click', async() =>{ 
    const {ip} = await getData(IP_URL)
    

    const {continent, country, region, city, district} = await getData(`${IP_API_URL}/${ip}?fields=status,message,continent,country,region,city,district`)
    const text = 'There is no available'
    const content = `
    <ul class="details">
        <li class="detail">Continent: ${continent || `${text} continent`}</li> 
        <li class="detail">Country: ${country || `${text} country`}</li> 
        <li class="detail">Region: ${region || `${text} region`}</li> 
        <li class="detail">City: ${city || `${text} city`}</li> 
        <li class="detail">District: ${district || `${text} district`}</li> 
    </ul>
    
    `
    const container = document.querySelector('.container')
    container.insertAdjacentHTML('beforeend', content)
    
    
    

/* continent, country, region, city, and district */

    btn.setAttribute('disabled', true)
})