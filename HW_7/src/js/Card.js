class Card {

    
    constructor(userData, postData, element) {
        this.name = userData.name
        this.postText = postData.body 
        this.postId = postData.id
        this.container = element
    }



    createCard() {
        const content = 
        `<li class="card" data-id="${this.postId}">
            <div class="card__container">
                <div class="card__header">
                    <img class="card__image" src="https://www.spacex.com/static/images/share.jpg" alt="${this.name}"/>
                    <div class="card__profile">
                        <h4>${this.name}</h4>
                        <p>@${this.name.replaceAll(' ', '')}</p>
                    </div>
                    <div class="three__dots">
                        <span class="dot"></span>
                        <span class="dot"></span>
                        <span class="dot"></span>
                    </div>    
                </div>

                <div class="card__body">
                    <p class="card__text">${this.postText}</p>
                    <p class="card__history">10:13 PM · Apr 6, 2022 · Twitter Web App</p>
                </div>


                <div class="card__footer">
                    <div class="card__info">
                    <strong>359</strong> Retweets
                    </div>
                    <div class="card__info">
                    <strong>31</strong> Quote Tweets
                    </div>
                    <div class="card__info">
                    <strong>2,878</strong> Likes
                    </div>
                </div>

                <button  type="button" class="btn-delete">Delete</button>
            </div>
        
        </li>`
        this.container.insertAdjacentHTML('beforeend', content)
    }

}

export default Card