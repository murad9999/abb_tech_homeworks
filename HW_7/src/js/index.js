
import Card from "./Card.js"
const USERS_URL = 'https://ajax.test-danit.com/api/json/users/'
const POSTS_URL = 'https://ajax.test-danit.com/api/json/posts/'

async function getData(url) {
    try {
        const res = await fetch(url)
        const data = await res.json()
        return data
    } catch (err) {
        throw new Error(err)
    }
}


const promises = [getData(USERS_URL), getData(POSTS_URL)]

async function getUsersAndPost() {

    try {
        const [users, posts] = await Promise.all(promises)

        return { users, posts }
    } catch (err) {
        throw new Error(err)
    }


}

function combineUsersAndPost(users, posts) {
    users.forEach(user => {
        const userPosts = posts.filter(post => post.userId === user.id)
        user.posts = userPosts
    })
}


function deletePost(e) {

    const deleteBtn = e.target.closest('.btn-delete');
    if (deleteBtn) {
        const card = deleteBtn.parentNode.parentNode;
        const postId = card.getAttribute('data-id')


        fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
            method: 'DELETE'
        })
            .then(res => {
                if (res.ok) {
                    card.remove();
                } else {
                    throw new Error(`Card can't be deleted `);
                }
            })
            .catch(err => {
                throw new Error(err);
            });
    }
}




getUsersAndPost().then(res => {
    const { users, posts } = res
    combineUsersAndPost(users, posts)
    const tweetsContainer = document.querySelector('.twitters')
    users.map(user => {
        user.posts.map(post => {
            const card = new Card(user, post, tweetsContainer)
            card.createCard()
        })
    })

    tweetsContainer.addEventListener('click', deletePost)
}).catch(err => {
    throw new Error(err)
})



